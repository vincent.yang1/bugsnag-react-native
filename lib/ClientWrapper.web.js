Object.defineProperty(exports,"__esModule",{value:true});var _js=require('@bugsnag/js');var _js2=_interopRequireDefault(_js);
var _util=require('util');var _util2=_interopRequireDefault(_util);function _interopRequireDefault(obj){return obj&&obj.__esModule?obj:{default:obj};}

var client=void 0;exports.default=

{
startWithOptions:function startWithOptions(options){
client=(0,_js2.default)(options);
},
notify:function notify(payload){
return _util2.default.promisify(client.notify(payload));
},
setUser:function setUser(_ref){var id=_ref.id,name=_ref.name,email=_ref.email;
client.user={id:id,name:name,email:email};
},
clearUser:function clearUser(){
client.user=null;
},
startSession:function startSession(){
client.startSession();
},
stopSession:function stopSession(){
client.stopSession();
},
resumeSession:function resumeSession(){
client.resumeSession();
},
leaveBreadcrumb:function leaveBreadcrumb(_ref2){var name=_ref2.name,type=_ref2.type,metadata=_ref2.metadata;
client.leaveBreadcrumb(name,metadata);
}};
//# sourceMappingURL=ClientWrapper.web.js.map